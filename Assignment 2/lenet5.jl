### A Pluto.jl notebook ###
# v0.14.7

using Markdown
using InteractiveUtils

# ╔═╡ 897ec334-de84-11eb-07ed-f9a7e398be73
using Pkg; Pkg.add(["Flux","MLDatasets","CUDA","BSON","ProgressMeter","TensorBoardLogger"]); GC.gc()

# ╔═╡ 11355bab-2a41-479b-9eb7-1404cb691eec
 Pkg.add(["FileIO","Images","ImageMagick","PlutoUI"]); GC.gc()

# ╔═╡ 7acd75a3-7a41-4a1a-88cb-94cac12f6dc4
begin
	
using Flux
using Flux.Data: DataLoader
using Flux.Optimise: Optimiser, WeightDecay
using Flux: onehotbatch, onecold
using Flux.Losses: logitcrossentropy
using Statistics, Random
using Logging: with_logger
using TensorBoardLogger: TBLogger, tb_overwrite, set_step!, set_step_increment!
using ProgressMeter: @showprogress
import MLDatasets
import BSON

using Images, FileIO
	using PlutoUI

GC.gc()
	
end

# ╔═╡ dd6c09ca-f891-48f3-be24-cccd81881c79

# Author: Bill Ikela 
# gitlab: https://gitlab.com/The-pixie-being


# ╔═╡ 0c14a5c4-ece0-43db-96aa-8b9021432a2b
md"### install required packages"

# ╔═╡ 43d5999c-349f-4289-a943-9309b024b94e
md"### precompile them"

# ╔═╡ 58408b27-270c-4675-bb5c-e160c173eafd
md"### get file paths"

# ╔═╡ d5e16e8f-5d2e-4af3-82d5-83a699d69713
begin
	abs = pwd()

    normal_train = abs*"/chest_xray/train/NORMAL/".*readdir(abs*"/chest_xray/train/NORMAL")[1:30]
	pneumonia_train = abs*"/chest_xray/train/PNEUMONIA/".*readdir(abs*"/chest_xray/train/PNEUMONIA")[1:30]

    normal_test = abs*"/chest_xray/test/NORMAL/".*readdir(abs*"/chest_xray/test/NORMAL")[1:30]
	pneumonia_test = abs*"/chest_xray/test/PNEUMONIA/".*readdir(abs*"/chest_xray/test/PNEUMONIA")[1:30]
	
end

# ╔═╡ c65e9ef6-b8fc-4699-832f-5fffd7fa1624
md"### load data"

# ╔═╡ 7250e599-a326-423f-9b66-4bc998ee6781
md"### just some utility functions"

# ╔═╡ a905048f-644e-4419-9504-38c511c91519
begin
	
	## utility functions
num_params(model) = sum(length, Flux.params(model)) 
round4(x) = round(x, digits=4)


#accepts array (v) and string(str), returns array of specified string with the length of str found in v
function labels(v,str,rturn=str)
    temp = []
    for (indx,i) in enumerate(v)
        if occursin(str,v[indx])
            push!(temp,rturn)
        end
    end
    return temp
end

#gets path, loads image,converts it to gray,resize it to 32*32 and float, then reshapes it to match model input
function process_image(path)
    img = load(path)
    img = Gray.(img)
    img = imresize(img,(32,32))
    img = Flux.unsqueeze(Float32.(img), 3)
    
    return img
end

	
end

# ╔═╡ f1fcd0a8-1a1d-4b0c-bbe4-36b5428b4d9b
function get_data()


    xtrain = [normal_train;pneumonia_train]
    xtest = [normal_test;pneumonia_test]


p_train_labels = labels(xtrain,"PNEUMONIA","pneumonia")
normal_train_labels=labels(xtrain,"NORMAL","normal")

p_test_labels=labels(xtest,"PNEUMONIA","pneumonia")
normal_test_labels=labels(xtest,"NORMAL","normal")


ytrain=vcat(normal_train_labels,p_train_labels)

ytest=vcat(normal_test_labels,p_test_labels)

    

xtrain =  [process_image.(x) for x in  xtrain]


xtest =  [process_image.(x) for x in  xtest]

    
Dict(:xtrain => xtrain,:xtest =>xtest ,:ytrain => ytrain ,:ytest =>ytest)
end

# ╔═╡ 6d5290c3-0987-4db8-98a1-4407437d6043
md"### Data preparation"

# ╔═╡ dea7555b-9ba8-4cea-bdaa-6646dfb068ea
function process_data()
    
#combine xtrain images into one array
xtrain = get_data()[:xtrain]
v= xtrain[1] #add first image
for i in 2:length(xtrain) #start from the 2nd image
   v = hcat(v,xtrain[i])
end

xtrain = reshape(v,32,32,1,:)

#do the same for xtest
xtest = get_data()[:xtest]
v2= xtest[1]
for i in 2:length(xtest)
   v2 = hcat(v2,xtest[i])
end

xtest = reshape(v2,32,32,1,:)



ytrain = get_data()[:ytrain]
ytest = get_data()[:ytest]
    
ytrain, ytest = onehotbatch(ytrain, ["normal","pneumonia"]), onehotbatch(ytest, ["normal","pneumonia"])

    train_loader = DataLoader((xtrain,ytrain), shuffle=true,batchsize=2)
    test_loader = DataLoader((xtest,ytest),  batchsize=2)


    #train_loader = DataLoader((xtrain, ytrain), batchsize=args.batchsize, shuffle=true)
    #test_loader = DataLoader((xtest, ytest),  batchsize=args.batchsize)
    
    return train_loader, test_loader
end

# ╔═╡ fe91a2aa-f907-46c2-9125-425cd114321b
md"### A mosaic view of 10 normal train images"

# ╔═╡ 4ae2ca54-69c3-4734-a7b7-a3899dda3caa

mosaicview([imresize(load(x),128,128) for x in  [normal_train;pneumonia_train]][1:10]; fillvalue=0.5, npad=5, ncol=5, rowmajor=true)


# ╔═╡ 30b714b3-6fa5-4c2b-b156-44a2c873310f
md"### 10 pneumonia (test) images"

# ╔═╡ 45f15e3f-b6f0-4bcc-a605-7650ff5f0082

mosaicview([imresize(load(x),128,128) for x in  pneumonia_test][1:10]; fillvalue=0.5, npad=5, ncol=5, rowmajor=true)


# ╔═╡ 30b5369a-02bb-496a-9e81-39f1bfcb9d30
md"### the LeNet5 architecture implementation"

# ╔═╡ 0267e7aa-2c1d-4e0d-b1ca-6b73985e876a

function LeNet5(; imgsize=(32,32,1), nclasses=2) 
    out_conv_size = (imgsize[1]÷4 - 3, imgsize[2]÷4 - 3, 16)
    
    return Chain(
        Conv((5, 5), imgsize[end]=>6, tanh),#; stride=1, pad=2), #just trying to modify pads manually 
            MeanPool((2, 2)),
            Conv((5, 5), 6=>16, tanh),
            MeanPool((2, 2)),
            flatten,
            Dense(prod(out_conv_size), 120, tanh), 
            Dense(120, 84, tanh), 
            Dense(84, 2), softmax
          )
    
    
end

# ╔═╡ b9687ab7-9e9f-4ddf-900d-b530853dff6e
loss(ŷ, y) = logitcrossentropy(ŷ, y)

# ╔═╡ ac74d9b4-1c15-42a5-b17c-8421bb8c5c7d
function eval_loss_accuracy(loader, model)
    l = 0f0
    acc = 0
    ntot = 0
    for (x, y) in loader
        ŷ = model(x)
        l += loss(ŷ, y) * size(x)[end]        
        acc += sum(onecold(ŷ) .== onecold(y))
        ntot += size(x)[end]
    end
    return (loss = l/ntot |> round4, acc = acc/ntot*100 |> round4)
end

# ╔═╡ e6a1c174-b74a-45e4-b165-017b1d6ce2f4
md"### main trainer function"

# ╔═╡ 443f3f3c-964a-40fe-8849-d1247a931f9c
function train()
   
    λ = 0                # L2 regularizer param, implemented as weight decay
    epochs = 10          # number of epochs
    seed = 0             # set seed > 0 for reproducibility
    infotime = 1 	     # report every `infotime` epochs
    checktime = 10        # Save the model every `checktime` epochs. Set to 0 for no checkpoints.
    savepath = "~/models/"    # results path
    
    
    ## DATA
    train_loader, test_loader = process_data()
    println( "Dataset chest-xray: $(train_loader.nobs) train and $(test_loader.nobs) test examples")

	
    ## MODEL AND OPTIMIZER
    model = LeNet5() 
    println( "LeNet5 model: $(num_params(model)) trainable params\n\n" )   
    
    ps = Flux.params(model)  

    opt = ADAM(3e-4) 
    if λ > 0 # add weight decay, equivalent to L2 regularization
        opt = Optimiser(WeightDecay(λ), opt)
    end
   
    
        function report(epoch)
        train = eval_loss_accuracy(train_loader, model)
        test = eval_loss_accuracy(test_loader, model)        
        println("Epoch: $epoch   \nTrain: $(train)  \nTest: $(test)\n\n")
        
    	end
    
    
    ## TRAINING
    println("Training model ...\n")
	
    report(0)
    for epoch in 1:epochs
        for (x, y) in train_loader
            gs = Flux.gradient(ps) do
                    ŷ = model(x)
                    loss(ŷ, y)
                end

            Flux.Optimise.update!(opt, ps, gs)
        end
                
        ## Printing and logging
        epoch % infotime == 0 && report(epoch)
        if checktime > 0 && epoch % checktime == 0
            !ispath(savepath) && mkpath(savepath)
            modelpath = joinpath(savepath, "model.bson") 
            let model = cpu(model) #return model to cpu before serialization
                BSON.@save modelpath model epoch
            end
            println("Model saved in \"$(modelpath)\"\n")
        end
    end
end



# ╔═╡ dc1e7c4f-ccad-415b-b5a1-0d0b4d8b4f0a
md"### let's run the model"

# ╔═╡ 2e2fb7b7-e7aa-43db-9f32-82688262f82f
with_terminal() do
	train()
end

# ╔═╡ Cell order:
# ╠═dd6c09ca-f891-48f3-be24-cccd81881c79
# ╠═0c14a5c4-ece0-43db-96aa-8b9021432a2b
# ╠═897ec334-de84-11eb-07ed-f9a7e398be73
# ╠═11355bab-2a41-479b-9eb7-1404cb691eec
# ╠═43d5999c-349f-4289-a943-9309b024b94e
# ╠═7acd75a3-7a41-4a1a-88cb-94cac12f6dc4
# ╠═58408b27-270c-4675-bb5c-e160c173eafd
# ╠═d5e16e8f-5d2e-4af3-82d5-83a699d69713
# ╠═c65e9ef6-b8fc-4699-832f-5fffd7fa1624
# ╠═f1fcd0a8-1a1d-4b0c-bbe4-36b5428b4d9b
# ╠═7250e599-a326-423f-9b66-4bc998ee6781
# ╠═a905048f-644e-4419-9504-38c511c91519
# ╠═6d5290c3-0987-4db8-98a1-4407437d6043
# ╠═dea7555b-9ba8-4cea-bdaa-6646dfb068ea
# ╠═fe91a2aa-f907-46c2-9125-425cd114321b
# ╠═4ae2ca54-69c3-4734-a7b7-a3899dda3caa
# ╠═30b714b3-6fa5-4c2b-b156-44a2c873310f
# ╠═45f15e3f-b6f0-4bcc-a605-7650ff5f0082
# ╠═30b5369a-02bb-496a-9e81-39f1bfcb9d30
# ╠═0267e7aa-2c1d-4e0d-b1ca-6b73985e876a
# ╠═b9687ab7-9e9f-4ddf-900d-b530853dff6e
# ╠═ac74d9b4-1c15-42a5-b17c-8421bb8c5c7d
# ╠═e6a1c174-b74a-45e4-b165-017b1d6ce2f4
# ╠═443f3f3c-964a-40fe-8849-d1247a931f9c
# ╠═dc1e7c4f-ccad-415b-b5a1-0d0b4d8b4f0a
# ╠═2e2fb7b7-e7aa-43db-9f32-82688262f82f
