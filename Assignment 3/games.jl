### A Pluto.jl notebook ###
# v0.14.7

using Markdown
using InteractiveUtils

# ╔═╡ 7c96a585-6daa-463b-abbc-2d1bf5cec167
using Pkg; Pkg.add("PlutoUI"); using PlutoUI

# ╔═╡ 4c73744c-0bcf-4e8b-905e-68857388fc20
md"###### author: Bill Ikela
###### [gitlab repo] (https://gitlab.com/The-pixie-being/aig/-/tree/main/Assignment%203)"

# ╔═╡ baa79a92-dfa8-11eb-3090-bf10bdde76a4
md"Write a program in the Julia programming language that:
- captures a sequential game between two players in its normal form 
- and computes each player’s payoff using a mixed strategy.
"

# ╔═╡ 8eb67d03-fa15-4109-8e4d-834d948cdc77
md"### Define player type"

# ╔═╡ 37622b92-d270-413c-931a-652e2a6cbba1

#define player type
mutable struct player
	payoff#::Matrix{Float32}
	actions#::Vector{Symbol}
	bimat#::Matrix{Tuple{Symbol,Float32}}


        #create default player #0.000005 seconds (4 allocations: 272 bytes)
    player(i::Bool) = begin
        payoff=Matrix{Float32}(undef,0,0)
        actions=Vector{Symbol}(undef,0)
        bimat=Matrix{Tuple{Symbol,Float32}}(undef,0,0)
        
       new(payoff,actions,bimat)
    end
    
    
    #create player given actions #  0.000007 seconds (2 allocations: 128 bytes)
    player(actions::Vector{Symbol}) = new(nothing,actions,nothing)
      #=  begin 
            x=new();   x.actions=actions; return x
        end
    =#
    #create player given payoff #  0.000003 seconds (2 allocations: 128 bytes)
    player(payoff::Matrix{Float64}) = new(payoff,nothing,nothing)
    #=    begin 
            x=new();   x.payoff=payoff; return x
        end
    =#
    
    #create player given payoff and actions 0.000003 seconds (3 allocations: 224 bytes)
    player(payoff::Matrix{Float64},actions::Vector{Symbol}) = new(payoff,actions,nothing) 
     #=   begin 
            x=new();   x.actions=actions; x.payoff=payoff; return x
        end
    =#
    
    #create player with empty fields
    player() = new(nothing,nothing,nothing) #0.000001 seconds (1 allocation: 32 bytes)
    #player() = new() #0.000001 seconds (1 allocation: 32 bytes)

end

# ╔═╡ c3bb6686-4658-48ad-9871-9c588e273f37
md"### Define normal game type"

# ╔═╡ 0c93a270-8aae-49de-99a2-ad1270cbc91e
begin

struct normgame
	payoff::Matrix
	players::Vector
#    gametype::Symbol

    #inner constructors
	normgame() = new()
    
    #accept payoff only
    normgame(payoff::Matrix{}) = 
    begin
       # p1=player(); p1.payoff=reshape(payoff[1:Int64(length(payoff)/2)],2,:)
       # p2=player(); p2.payoff=reshape(payoff[Int64(length(payoff)/2+1):length(payoff)],2,:)
       # new(reshape(payoff,2,:),[p1,p2])
        
        if checklength(payoff)
            ply = createPlayers(length(payoff[1]))#players vectors
            for tupl in payoff
                #println(tupl[1])
                for i = 1:length(tupl)
                 # println("player $i =>",tupl[i])
                    push!(ply[i].actions,tupl[i])
                end
            end
               new(reshape(payoff,2,:),ply) 
        end
    end
    
    #convert payoff of int to float
    normgame(payoff::Matrix{Int64}) = normgame(Float64.(payoff))
   
     normgame(payoff::Matrix{Tuple{Int64,Int64}}) = 
       begin
        mat = []
            for i in 1:length(payoff)
                push!(mat, Float64.(payoff[i]))
            end
        normgame(reshape(mat,2,:))
        end 
    #accept players with payoffs: then create a game
    normgame(players::Vector{player}) = 
    begin
        temp =[]
        #loop thru players and get their payoffs
        for p in players
            if p.payoff == nothing && p.actions == nothing
                throw("players should have atleats payoffs") 
                #return new(Matrix{Float32}(undef,0,0),Vector())
            else
                push!(temp,reshape(p.payoff,1,:))#convert to vector and add to temp
            end
        end
        
        
        new(toPayoff(temp),players)#create game instance
    end
   
        
end

	
	
	
	
	#create players according to payoff vector
function createPlayers(i)
    v = []
    for i = 1:i
       push!(v,player([:o]))
        v[i].actions = Vector()
    end
    return v
end
#--------------------------------------------------------#
# create players with payoff matrices : the number of players = vector length
function createPlayersmat(i)
    v = []
    for i = 1:i
       push!(v,player())
        v[i].payoff = Matrix{Float32}(undef,0,0)
    end
    return v
end

#--------------------------------------------------------#
#checks if the payoff matrix/vector is even and if their payoffs are of equal count
function checklength(vec)::Bool
if length(vec)%2 != 0
    throw("payoff matrix should be even")
end

for i in 2:length(vec)
    if length(vec[i]) != length(vec[i-1])
            throw("players should have equal number of payoffs")
    end
end
    return true
end

#--------------------------------------------------------#
# returns a tuple with an aditional element; gets a tuple and an elemnent to be added
function tupleMerge(t,x)
    v=[] #temp vector
    #extract tuple elements and add em to v
    for i in t
        push!(v,i)
    end
    #add x to v
    push!(v,x)
    #copy v elements wrapped in a tuple
    v = [Tuple(deepcopy(v))]
    return v[1]#return the tuple
end

#--------------------------------------------------------#
#returns a matrix of payoffs when given a vector instead
function toPayoff(vecx)
mat = [] #temp vector

#the first vector/player
    for i = 1:length(vecx[1])
        push!(mat,tupleMerge((),vecx[1][i]))
    end
#for the rest of the vectors/players
for v in 2:length(vecx)
    
    for i = 1:length(vecx[v])
          
        mat[i] = tupleMerge(mat[i],vecx[v][i]) #join tuples
    end
end    
reshape(mat,2,:) #to matrix
#    return mat
end

#-------------------------------------------------------------------------#
 #function to randomly shuffle array elements
 function shuffle!(arr)

    ind = rand(1:length(arr))
    checked = Any[]

      for i = 1:ind

         done = false
         chkd = true

         while !done
	
	        if length(checked) >= length(arr)
                return arr
            end

           tmp2 = rand(1:length(arr))
           tmp = rand(1:length(arr))

            while chkd
                   if !does_it_exist(checked, tmp)

                       if !does_it_exist(checked, tmp2)

                           chkd = false


                       else
                           tmp2 = rand(1:length(arr))
                       end

                   else
                       tmp = rand(1:length(arr))
                   end
               end


                if tmp != tmp2
#print(tmp)
                   arr[tmp],arr[tmp2] = arr[tmp2], arr[tmp]
                   push!(checked,tmp,tmp2)
                   done = true

               end
            end
          end

     return arr
 end

#-------------------------------------------------------------------------#
#function that checks if a given element exists in the array
function does_it_exist(arr, idx)
found = false

	if length(arr) < 1
               return found

           else
           
               for i = 1:length(arr)
                   if idx == arr[i]
                       return true

                   end
               end
           end
           return found
end

#--------------------------------------------------------#
#displays all payoffs in a given game
function displayPayoffs(v)#will change variable v name prolly
    for (indx,tupl) in enumerate(v)
        println("step :: $indx \n")
        for (i,payoff) in enumerate(tupl)
            println("player $i payoff => $payoff")
        end
        println("\n")
    end
end
#--------------------------------------------------------#

function getDominant(game)
    #code here
end
#--------------------------------------------------------#
#displays payoffs a number of times (steps) according to a specified stratergy
function play(game,steps=:max,stratergy =:mixed)
    
    
    
    if typeof(steps) == Int64 && steps > length(game.payoff)
        #steps = length(game.payoff)
        
        @info "steps exceed the number of payoffs, reducing steps to ",length(game.payoff)
        
        sleep(0.100)
        return displayPayoffs(shuffle!(game.payoff))
    elseif steps ==:max
        steps = length(game.payoff)
    end
    
    if stratergy == :mixed
        
        println("game type :: ", typeof(game) == normgame ? "normal form game" : "action form game" )
        #println(typeof(game) == normgame? "normal form game":"action form game" )
       
        #println("payoff matrix :: ",game.payoff)
        println("number of players :: ",length(game.players))
			 println("payoff matrix :: ",game.payoff,"\n\n")
        #println("player actions :: ",game.payoff,"\n\n")
        
        
		prechecked = []
			i = 1
       while steps > 0
            
					tupl_indx = rand(1:length(game.payoff))#take a random tuple
				
				#make sure you don't print same payoff
				if tupl_indx in prechecked
					#steps += 1
					continue
				else
					tupl = game.payoff[tupl_indx]
					push!(prechecked,tupl_indx)
				
            		println("step :: $i \n")
       			     #loop thru players
       			     for p in 1:length(tupl)
        		        println("player $p payoff => ", tupl[p])#print players payoffs
         			   end
             println("\n")# pretty line print :XD
				end
				steps -= 1#decrement steps
				i += 1
        end
           
    
    elseif stratergy == :pure
        displayPayoffs(game.payoff)
    elseif stratergy == :dominant
        getDominant(game)
    end
    
end

	

	
	
	
	
	
end

# ╔═╡ b61b6b40-2752-4f41-90c2-5ce563046c51
md"### Utility functions

##### supposed to be below, but pluto is infected by covid, complains too much about cyclic reference, so i put them above with normgame definition"

# ╔═╡ 9169ccea-5c1d-4dfa-8705-09025fb2a1ab


# ╔═╡ 5f087984-f0af-4e56-a46c-a3237ee7a4b4
md"### Lets play some GAMES"

# ╔═╡ 1c7327bf-ddcc-49d5-9c18-0f0a4fc748d1
md"#### battle of the sexes"

# ╔═╡ 91ddcbb0-5ee6-459f-859c-4aa348786922
begin
	
	md"## battle of the sexes"
payoff1 = [(3,2) (0,0);(1,1) (2,3)]
game1 = normgame(payoff1)
	with_terminal() do
		play(game1,3)
	end
end

# ╔═╡ c31c3610-1f3c-4ac9-848a-8e188b95fc77
md"#### sequential prisoner's dilemma"

# ╔═╡ be46a119-4dc5-447c-aeda-6118f8c39e36
begin
	### sequential prisoner's dilemma
vecx = [[5.0, 8.0, 7.0, 4.0],[8.0, 5.0, 4.0, 7.0],] #vecor of payofs
#create players
p1 = player(reshape(vecx[1],2,:))
p2 = player(reshape(vecx[2],2,:))

players = [p1,p2]
game = normgame(players)
	with_terminal() do
		play(game)
	end
end

# ╔═╡ 5b5fba7b-191b-4bde-b5ba-f344cde912fc
md"#### ultimatum/dictator game
the dictator has $10 to split:
so the payoff depends on player 2's decision.

it looks like this:

(10,0) (9,1) (8,2) (7,3) (6,4) (5,5) (4,6) (3,7) (2,8) (1,9) (0,10) (0,0)
"

# ╔═╡ b4070158-4b00-4d56-b6ce-8a4ac70172e9
begin
	###dictator game

payoff = [(10,0) (9,1) (8,2) (7,3) (6,4) (5,5) (4,6) (3,7) (2,8) (1,9) (0,10) (0,0)]
payoff= reshape(payoff,2,:)
#normgame(payoff)
with_terminal() do
		play(normgame(payoff),8)
	end
end

# ╔═╡ ff676c84-6864-4957-bf48-fc7d569d45f2
md"##### pluto is ok,BUT still an infant, i lOvE JuPyTeR"

# ╔═╡ Cell order:
# ╠═4c73744c-0bcf-4e8b-905e-68857388fc20
# ╠═baa79a92-dfa8-11eb-3090-bf10bdde76a4
# ╠═7c96a585-6daa-463b-abbc-2d1bf5cec167
# ╠═8eb67d03-fa15-4109-8e4d-834d948cdc77
# ╠═37622b92-d270-413c-931a-652e2a6cbba1
# ╠═c3bb6686-4658-48ad-9871-9c588e273f37
# ╠═0c93a270-8aae-49de-99a2-ad1270cbc91e
# ╠═b61b6b40-2752-4f41-90c2-5ce563046c51
# ╠═9169ccea-5c1d-4dfa-8705-09025fb2a1ab
# ╠═5f087984-f0af-4e56-a46c-a3237ee7a4b4
# ╠═1c7327bf-ddcc-49d5-9c18-0f0a4fc748d1
# ╠═91ddcbb0-5ee6-459f-859c-4aa348786922
# ╠═c31c3610-1f3c-4ac9-848a-8e188b95fc77
# ╠═be46a119-4dc5-447c-aeda-6118f8c39e36
# ╠═5b5fba7b-191b-4bde-b5ba-f344cde912fc
# ╠═b4070158-4b00-4d56-b6ce-8a4ac70172e9
# ╠═ff676c84-6864-4957-bf48-fc7d569d45f2
