### A Pluto.jl notebook ###
# v0.14.7

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : missing
        el
    end
end

# ╔═╡ 467d7342-df91-11eb-1a4f-378c25a1d058
begin
	using Pkg; Pkg.add("PlutoUI")
  cd("/home/jovyan")
	using Random
	using Printf
	using PlutoUI
	
end

# ╔═╡ acf97c6a-14cb-49a2-914b-80efc3a114d0
md"###### Author: Bill Ikela

[gitlab repo](https://gitlab.com/The-pixie-being/aig/-/tree/main/Assignment%203)"

# ╔═╡ eca5cb13-6f8c-457c-bee6-f9655be41e16
md"### The problem
Your task in this problem is to write a program in the Julia programming
language that performs the following:
- capture and store a Markov decision process (MDP) at runtime;
- given a policy, evaluate the policy and improve it to get a solution.
"

# ╔═╡ 2e17286a-f029-4baa-ba95-f0a6020ac29f
md"### Define MDP and policy strucs"

# ╔═╡ 26be460f-3f0c-40ca-b7b6-bff2d2b1b0d1
begin
abstract type MDP{S,A} end

abstract type Policy end

function states end
function initialstate end
function stateindex end
isterminal(problem::MDP, state) = false

function actionindex end
function actions end
actions(problem::MDP, state) = actions(problem)

function discount end
function transition end
function reward end
reward(m::MDP, s, a, sp) = reward(m, s, a)
end

# ╔═╡ 1bed67da-53fa-49eb-9d76-54bfb74fd0a7
begin
	abstract type Solver end
	function solve end
	
	function action end
	function updater end
	function value end

	
##type inferrence

statetype(t::Type) = statetype(supertype(t))
statetype(t::Type{MDP{S,A}}) where {S,A} = S
 statetype(p::MDP) = statetype(typeof(p))
include("common.jl")
actiontype(t::Type) = actiontype(supertype(t))
actiontype(t::Type{MDP{S,A}}) where {S,A} = A
actiontype(p::MDP) = actiontype(typeof(p))
end

# ╔═╡ 9fd22301-4229-43e2-bd03-f3662d90f683
md"### policy and states utility functions"

# ╔═╡ 7e07339e-a423-4373-8148-26840c0835ae
md"### The policy solver

###### I used value iteration function to find the optimal policy, i.e policy solver and evalute function in one"

# ╔═╡ 2be2f714-df28-4225-9904-9df2bf8202e1
md"### Distributions"

# ╔═╡ 66271f23-cef2-438e-b529-998eabcfb23d
begin
	
struct Deterministic{T}
    val::T
end

rand(rng::AbstractRNG, d::Deterministic) = d.val
rand(d::Deterministic) = d.val
support(d::Deterministic) = (d.val,)
sampletype(::Type{Deterministic{T}}) where T = T
Random.gentype(::Type{Deterministic{T}}) where T = T
pdf(d::Deterministic, x) = convert(Float64, x == d.val)
mode(d::Deterministic) = d.val
mean(d::Deterministic{N}) where N<:Number = d.val / 1 # / 1 is to make this return a similar type to Statistics.mean
mean(d::Deterministic) = d.val # so that division need not be implemented for the value type
	
end

# ╔═╡ 92c62b95-c6bf-47fe-968e-6042fe4b1583


# ╔═╡ 0a36d8a7-5c39-4a25-8ef2-27649e4900a5

md"### The MDP problem definition

##### i used the classic gridworld problem"

# ╔═╡ 72a09b23-6b6d-462f-8572-5288dd43c88b
begin
	
	struct GridWorldState
    x::Int64 # x position
    y::Int64 # y position
    done::Bool # are we in a terminal state?
end
	
	
	
	
	# initial state constructor
GridWorldState(x::Int64, y::Int64) = GridWorldState(x,y,false)
# checks if the position of two states are the same
posequal(s1::GridWorldState, s2::GridWorldState) = s1.x == s2.x && s1.y == s2.y
end

# ╔═╡ 6fce0b36-6cb7-48c5-8f67-eecb92f6ab52
begin
	
# the grid world mdp type
mutable struct GridWorld <: MDP{GridWorldState, Symbol} 
    size_x::Int64 # x size of the grid
    size_y::Int64 # y size of the grid
    reward_states::Vector{GridWorldState} # the states in which agent recieves reward
    reward_values::Vector{Float64} # reward values for those states
    tprob::Float64 # probability of transitioning to the desired state
    discount_factor::Float64 # disocunt factor
end
	
	
	 
function GridWorld(;sx::Int64=10, # size_x
                    sy::Int64=10, # size_y
                    rs::Vector{GridWorldState}=[GridWorldState(4,3), GridWorldState(4,6), GridWorldState(9,3), GridWorldState(8,8)], # reward states
                    rv::Vector{Float64}=rv = [-10.,-5,10,3], # reward values
                    tp::Float64=0.7, 
                    discount_factor::Float64=0.9)
    return GridWorld(sx, sy, rs, rv, tp, discount_factor)
end

mdp = GridWorld()
mdp.reward_states # mdp contains all the defualt values from the constructor
	
end

# ╔═╡ 87cfc712-3e32-4ae0-b75b-74f29f760c84
begin
	
	function states(mdp::GridWorld)
    s = GridWorldState[] # initialize an array of GridWorldStates
    # loop over all our states, remeber there are two binary variables:
    # done (d)
    for d = 0:1, y = 1:mdp.size_y, x = 1:mdp.size_x
        push!(s, GridWorldState(x,y,d))
    end
    return s
end;
	
end

# ╔═╡ 0b59f619-b7fb-44d8-b672-8351bfd21006
md"###### ensures the agent does not go beyond the grid"

# ╔═╡ d8c40f78-c3ae-42a6-96b9-f78cf9c88dd4
begin
	
function inbounds(mdp::GridWorld,x::Int64,y::Int64)
    if 1 <= x <= mdp.size_x && 1 <= y <= mdp.size_y
        return true
    else
        return false
    end
end

inbounds(mdp::GridWorld, state::GridWorldState) = inbounds(mdp, state.x, state.y);
	
	
end

# ╔═╡ 53f452c5-77a4-410f-a4d0-a4b7439ede1f
md"### The transition function"

# ╔═╡ 3f094c65-653e-4352-b6bd-9ac6ffcfeb7b
begin
function reward(mdp::GridWorld, state::GridWorldState, action::Symbol, statep::GridWorldState) #deleted action
    if state.done
        return 0.0
    end
    r = 0.0
    n = length(mdp.reward_states)
    for i = 1:n
        if posequal(state, mdp.reward_states[i])
            r += mdp.reward_values[i]
        end
    end
    return r
end;	
end

# ╔═╡ 660275e1-62a4-465c-a082-8837c8048177
discount(mdp::GridWorld) = mdp.discount_factor;

# ╔═╡ b581d278-e0cd-43d2-abf3-a3cc3e539e36
md"##### helper functions for states and actions"

# ╔═╡ 37491608-c0e6-46ae-8086-4c8c75071d31
isterminal(mdp::GridWorld, s::GridWorldState) = s.done

# ╔═╡ 2d771040-f8ab-4518-b50f-17318dc3f359
md"#### get initial state"

# ╔═╡ 1726bbb4-3da4-41f5-b9eb-08004da2c646
initialstate(mdp::GridWorld) = Deterministic(GridWorldState(1,1)) 

# ╔═╡ 81f90122-7e64-4cce-86ee-a97ace6c783a


# ╔═╡ 2b3fe348-f8c0-4339-a828-8cfd579fb557
md"### more utility functions"

# ╔═╡ 745a1c95-f9a0-4383-9531-69b0f124a9b3
function prompt(str)
    print(str)
	@bind out TextField()
    return  out 
end

# ╔═╡ 95587475-6f74-4ae9-854d-e928cb9be32f
md"### MDP problem interface to accpet input in runtime"

# ╔═╡ cac24655-5b90-48b7-8856-4808abf558ed
md"###### input number of states"

# ╔═╡ 04c4b17a-97aa-4133-bf59-e3045f69a95d
statecountin = "";@bind statecountin TextField()

# ╔═╡ ee939c1e-5f09-4924-a890-00282a39a868
begin	
	if isempty(statecountin)
		statecount = 100
		
		
#prompt states
#statecount =parse(Int64,prompt("how many states: "))
	#with_terminal() do
	#	print("how many states: ")
	#end
	
	#gridsize=Int64(round(sqrt(statecount)))

#mat=Matrix{Int64}(undef,1,statecount)
#for i = 1:statecount
#   mat[i] = i
#end
	else
		statecount= parse(Int64,statecountin)
	end
end

# ╔═╡ 8c9ff10c-e79a-432c-a872-bb7910022118
md"###### input actions separated by commas"

# ╔═╡ 4a7c5ed4-756f-434d-b7d1-c24a19598847
begin
actinput = ""
	@bind actinput TextField()
end

# ╔═╡ bde06c59-26e9-4330-a544-39c1a8b6cd5c
begin
	#act=[]
	#bind act TextField()
acts =[]
	
	if !isempty(actinput)
	fromact = split(actinput,",")
	with_terminal()do
for i = 1:length(fromact)
    #act =  prompt("enter action $i")
		
			println(fromact[i])
		
			
    act=Symbol(fromact[i])
    push!(acts,act)
		
			end
end

	else
		acts=[:up, :down, :left, :right]
	end
	
end

# ╔═╡ 978e1a13-4397-4504-bcc1-ba7914f3d990
acts

# ╔═╡ c0b6192b-a090-460a-b42c-8ae64acd8c2c
md"##### input transition probability"

# ╔═╡ c2849337-4291-44cf-b7a3-5dc00cb92fda
begin
	
#get transition probability
probin = ""
	#parse(Float64,prompt("enter the transition pobability: "))

@bind probin TextField()
end

# ╔═╡ 281e9ebd-5b69-4bfd-9024-8380eb3d94f0
md"##### input discount factor"

# ╔═╡ f9d20180-4b85-40b7-afa3-55c4394cb550
discin = ""; @bind discin TextField()

# ╔═╡ 7e20833a-36f9-4611-975e-200052a59183
begin
	if !isempty(discin)
disc = parse(Float64,discin)
	#parse(Float64,prompt("enter discount factor: "))
if 10>= disc > 1 
    disc = disc/10
elseif 100 >= disc > 10
    disc = disc/100
end
	else
		disc = 0.9
	end
end

# ╔═╡ a6c11704-e20e-4a7c-b66f-b0edba7cfd7d
begin
	if !isempty(probin)
		prob = parse(Float64,probin)
if 10>= prob > 1 
    prob = prob/10
elseif 100 >= disc > 10
    prob = prob/100
end
	else
		prob = 0.7
	end

end

# ╔═╡ 8c22b3bd-be8b-4f03-8d40-18e7257cd629
md"###### input reward states if any"

# ╔═╡ 1e2f6b70-8038-4199-9cb8-e6fc27b4a8a5
tupl = ();@bind tupl TextField()

# ╔═╡ 803e02d9-f616-4d96-8125-c63b395ea6b5
md"###### input rewards to match with reward states"

# ╔═╡ 09049c13-8fbc-4cee-8868-8d347ca6f999
rewardss = ();@bind rewardss TextField()

# ╔═╡ 58aee161-b606-430e-ad4c-0a5a1375b2ee


# ╔═╡ 121de529-4ff1-4e1b-aa19-7eb224d79719
begin
	
rs = [];rewards = []
	
	if !isempty(tupl)
	fromtupl=split(tupl,",")
for i in fromtupl
		x=parse(Int64,i)
	 push!(rs,GridWorldState(1,x))
	end
	else
		rs=[GridWorldState(1,12), GridWorldState(1,36), GridWorldState(1,27), GridWorldState(1,64)]
	end
		
		if !isempty(rewardss)
	
	fromrewardss = split(rewardss,",")
	
	
	for i in fromrewardss
		x=parse(Int64,i)
	push!(rewards,x)
	end
	else
		rewards = [-10.,-5,10,3]
		end

#reward states


#if prompt("is there any reward state (y|n): ") == "y"
 #   rscount =parse(Int64,prompt("how many reward states are there: "))
 #   for i = 1:rscount
        #tupl = Tuple(prompt("enter coordinates for reward state $i (e.g 4,2)"))
         #Tuple(prompt("enter state index for reward state $i (e.g 64 (if states >= 64"))
     
	
	
        #    x=parse(Int64,tupl[1])
            #y=parse(Int64,tupl[end])
            #cords=x,y
       #     push!(rs,GridWorldState(1,x))
        
      #  r = parse(Float64,prompt("enter reward value for reward state $i: "))
     #   push!(rewards,r)
    #end
#end

rs
	mymdp = GridWorld(1,statecount,rs,rewards,prob,disc)
	
end

# ╔═╡ 7105a075-3bd5-4337-b4d6-35c9e578bb5e


# ╔═╡ 1f127c90-d433-40d8-8cc9-a1b318976b33
md"### define actions"

# ╔═╡ 273eff70-7d6e-46ec-a367-fbbdb49ddcec
begin
mdp2 = GridWorld()
actions(mdp::GridWorld) = [:up, :down, :left, :right];
end


# ╔═╡ eb14513c-523e-438d-a79c-83ad05fda231
begin
function stateindex(mdp::GridWorld, state::GridWorldState)
    sd = Int(state.done + 1)
    ci = CartesianIndices((mdp.size_x, mdp.size_y, 2))
    return LinearIndices(ci)[state.x, state.y, sd]
end

function actionindex(mdp::GridWorld, act::Symbol)

    for (i,x) in enumerate(actions(mdp))
    if act ==x
            return i
        end
end
    
    error("Invalid GridWorld action: $act")
end;	
end

# ╔═╡ 2fd236ad-bace-4b1c-a116-51b5845b271e
function actionsDict(mdp)
temp = Dict()
for (i,x) in enumerate(actions(mdp))
    push!(temp,x=>i)
end
    return temp
end

# ╔═╡ f4dcfad1-6c93-4a45-9c14-68a41f3f8e01
begin
	function transition(mdp::GridWorld, state::GridWorldState, action::Symbol)
    a = action
    x = state.x
    y = state.y
    
    if state.done
        return SparseCat([GridWorldState(x, y, true)], [1.0])
    elseif state in mdp.reward_states
        return SparseCat([GridWorldState(x, y, true)], [1.0])
    end

    neighbors = [
        GridWorldState(x+1, y, false), 
        GridWorldState(x-1, y, false), 
        GridWorldState(x, y-1, false), 
        GridWorldState(x, y+1, false), 
        ] 
    
    targets = actionsDict(mdp)
    
    target = targets[a]
    
    probability = fill(0.0, 4)

    if !inbounds(mdp, neighbors[target])
        # If would transition out of bounds, stay in
        # same cell with probability 1
        return SparseCat([GridWorldState(x, y)], [1.0])
    else
        probability[target] = mdp.tprob

        oob_count = sum(!inbounds(mdp, n) for n in neighbors) # number of out of bounds neighbors

        new_probability = (1.0 - mdp.tprob)/(3-oob_count)

        for i = 1:4 # do not include neighbor 5
            if inbounds(mdp, neighbors[i]) && i != target
                probability[i] = new_probability
            end
        end
    end

    return SparseCat(neighbors, probability)
end;
end

# ╔═╡ 7150ec49-8fbf-4702-9480-a7387551c4ab
begin
	

# The solver type

mutable struct ValueIterationSolver <: Solver
    max_iterations::Int64 # max number of iterations
    belres::Float64 # the Bellman Residual
    verbose::Bool 
    include_Q::Bool
    init_util::Vector{Float64}
end
# Default constructor
function ValueIterationSolver(;max_iterations::Int64 = 100, 
                               belres::Float64 = 1e-3,
                               verbose::Bool = false,
                               include_Q::Bool = true,
                               init_util::Vector{Float64}=Vector{Float64}(undef, 0))    
    return ValueIterationSolver(max_iterations, belres, verbose, include_Q, init_util)
end


function solve(solver::ValueIterationSolver, mdp::MDP; kwargs...)
    

    # solver parameters
    max_iterations = solver.max_iterations
    belres = solver.belres
    discount_factor = discount(mdp)
    ns = length(states(mdp))
    na = length(actions(mdp))

    # intialize the utility and Q-matrix
    if !isempty(solver.init_util)
        @assert length(solver.init_util) == ns "Input utility dimension mismatch"
        util = solver.init_util
    else
        util = zeros(ns)
    end
    include_Q = solver.include_Q
    if include_Q
        qmat = zeros(ns, na)
    end
    pol = zeros(Int64, ns)

    total_time = 0.0
    iter_time = 0.0

    # create an ordered list of states for fast iteration
    state_space = ordered_states(mdp)

    # main loop
    for i = 1:max_iterations
        residual = 0.0
        iter_time = @elapsed begin
        # state loop
        for (istate,s) in enumerate(state_space)
            sub_aspace = actions(mdp, s)
            if isterminal(mdp, s)
                util[istate] = 0.0
                pol[istate] = 1
            else
                old_util = util[istate] # for residual
                max_util = -Inf
                # action loop
                # util(s) = max_a( R(s,a) + discount_factor * sum(T(s'|s,a)util(s') )
                for a in sub_aspace
                    iaction = actionindex(mdp, a)
                    dist = transition(mdp, s, a) # creates distribution over neighbors
                    u = 0.0
                    for (sp, p) in weighted_iterator(dist)
                        p == 0.0 ? continue : nothing # skip if zero prob
                        r = reward(mdp, s, a, sp)
                        isp = stateindex(mdp, sp)
                        u += p * (r + discount_factor * util[isp])
                    end
                    new_util = u
                    if new_util > max_util
                        max_util = new_util
                        pol[istate] = iaction
                    end
                    include_Q ? (qmat[istate, iaction] = new_util) : nothing
                end # action
                # update the value array
                util[istate] = max_util
                diff = abs(max_util - old_util)
                diff > residual ? (residual = diff) : nothing
            end
        end # state
        end # time
        total_time += iter_time
        solver.verbose ? @printf("[Iteration %-4d] residual: %10.3G | iteration runtime: %10.3f ms, (%10.3G s total)\n", i, residual, iter_time*1000.0, total_time) : nothing
        residual < belres ? break : nothing
    end # main
    

    if include_Q
        return ValueIterationPolicy(mdp, qmat, util, pol)
    else
        return ValueIterationPolicy(mdp, utility=util, policy=pol, include_Q=false)
    end
end
end

# ╔═╡ 09eab8b7-bb79-4646-9ddd-2be930dce4a5
md"### run the policy solver"

# ╔═╡ 7628014b-a5cd-44a5-9086-2a777b86078b
begin
	
# initialize the solver
solver = ValueIterationSolver(max_iterations=100, belres=1e-3; verbose=true)
policy = nothing
	
with_terminal() do
	policy = solve(solver, mymdp);
	end
end

# ╔═╡ e9c2ff49-4a97-4004-9664-58dda69ceb2b
md"#### test the policy"

# ╔═╡ 9264a6c9-f685-4bcd-b577-dc141e75fc1b
begin
	with_terminal() do
# say we are in state 9,
#test 9,5,17,
		for i = 1:mymdp.size_y
s = GridWorldState(1,i)
a = action(policy, s)
		
		println("state:$i =>",a)
		end
	end
end

# ╔═╡ ee25d984-f13c-475a-8206-4c4d58b53b6a


# ╔═╡ 9f2bdf7a-dd02-4485-8908-3d7125e57f98
#@bind x Slider(5:15)

# ╔═╡ 614c8216-0b8a-4eea-a103-ee358490dada


# ╔═╡ 85c40845-bbdc-4906-8640-92928dad5139
md" ### TODO
- add sparse distribution
- add function policy
- add binary mdp

"

# ╔═╡ Cell order:
# ╠═acf97c6a-14cb-49a2-914b-80efc3a114d0
# ╠═eca5cb13-6f8c-457c-bee6-f9655be41e16
# ╠═467d7342-df91-11eb-1a4f-378c25a1d058
# ╠═2e17286a-f029-4baa-ba95-f0a6020ac29f
# ╠═26be460f-3f0c-40ca-b7b6-bff2d2b1b0d1
# ╠═9fd22301-4229-43e2-bd03-f3662d90f683
# ╠═1bed67da-53fa-49eb-9d76-54bfb74fd0a7
# ╠═7e07339e-a423-4373-8148-26840c0835ae
# ╠═7150ec49-8fbf-4702-9480-a7387551c4ab
# ╠═2be2f714-df28-4225-9904-9df2bf8202e1
# ╠═66271f23-cef2-438e-b529-998eabcfb23d
# ╠═92c62b95-c6bf-47fe-968e-6042fe4b1583
# ╠═0a36d8a7-5c39-4a25-8ef2-27649e4900a5
# ╠═72a09b23-6b6d-462f-8572-5288dd43c88b
# ╠═6fce0b36-6cb7-48c5-8f67-eecb92f6ab52
# ╠═87cfc712-3e32-4ae0-b75b-74f29f760c84
# ╠═0b59f619-b7fb-44d8-b672-8351bfd21006
# ╠═d8c40f78-c3ae-42a6-96b9-f78cf9c88dd4
# ╠═53f452c5-77a4-410f-a4d0-a4b7439ede1f
# ╠═f4dcfad1-6c93-4a45-9c14-68a41f3f8e01
# ╠═3f094c65-653e-4352-b6bd-9ac6ffcfeb7b
# ╠═660275e1-62a4-465c-a082-8837c8048177
# ╠═b581d278-e0cd-43d2-abf3-a3cc3e539e36
# ╠═eb14513c-523e-438d-a79c-83ad05fda231
# ╠═37491608-c0e6-46ae-8086-4c8c75071d31
# ╠═2d771040-f8ab-4518-b50f-17318dc3f359
# ╠═1726bbb4-3da4-41f5-b9eb-08004da2c646
# ╟─81f90122-7e64-4cce-86ee-a97ace6c783a
# ╠═2b3fe348-f8c0-4339-a828-8cfd579fb557
# ╠═2fd236ad-bace-4b1c-a116-51b5845b271e
# ╠═745a1c95-f9a0-4383-9531-69b0f124a9b3
# ╠═95587475-6f74-4ae9-854d-e928cb9be32f
# ╠═cac24655-5b90-48b7-8856-4808abf558ed
# ╠═04c4b17a-97aa-4133-bf59-e3045f69a95d
# ╠═ee939c1e-5f09-4924-a890-00282a39a868
# ╠═8c9ff10c-e79a-432c-a872-bb7910022118
# ╠═4a7c5ed4-756f-434d-b7d1-c24a19598847
# ╠═bde06c59-26e9-4330-a544-39c1a8b6cd5c
# ╠═978e1a13-4397-4504-bcc1-ba7914f3d990
# ╠═c0b6192b-a090-460a-b42c-8ae64acd8c2c
# ╠═c2849337-4291-44cf-b7a3-5dc00cb92fda
# ╠═a6c11704-e20e-4a7c-b66f-b0edba7cfd7d
# ╠═281e9ebd-5b69-4bfd-9024-8380eb3d94f0
# ╠═f9d20180-4b85-40b7-afa3-55c4394cb550
# ╠═7e20833a-36f9-4611-975e-200052a59183
# ╠═8c22b3bd-be8b-4f03-8d40-18e7257cd629
# ╠═1e2f6b70-8038-4199-9cb8-e6fc27b4a8a5
# ╠═803e02d9-f616-4d96-8125-c63b395ea6b5
# ╠═09049c13-8fbc-4cee-8868-8d347ca6f999
# ╟─58aee161-b606-430e-ad4c-0a5a1375b2ee
# ╠═121de529-4ff1-4e1b-aa19-7eb224d79719
# ╟─7105a075-3bd5-4337-b4d6-35c9e578bb5e
# ╠═1f127c90-d433-40d8-8cc9-a1b318976b33
# ╠═273eff70-7d6e-46ec-a367-fbbdb49ddcec
# ╠═09eab8b7-bb79-4646-9ddd-2be930dce4a5
# ╠═7628014b-a5cd-44a5-9086-2a777b86078b
# ╠═e9c2ff49-4a97-4004-9664-58dda69ceb2b
# ╠═9264a6c9-f685-4bcd-b577-dc141e75fc1b
# ╟─ee25d984-f13c-475a-8206-4c4d58b53b6a
# ╟─9f2bdf7a-dd02-4485-8908-3d7125e57f98
# ╟─614c8216-0b8a-4eea-a103-ee358490dada
# ╠═85c40845-bbdc-4906-8640-92928dad5139
