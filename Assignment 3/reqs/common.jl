# The policy type
using Random
import Pkg;
Pkg.add("POMDPPolicies")

using POMDPPolicies


##ordered spaces
abstract type MDP{S,A} end

#https://github.com/JuliaPOMDP/POMDPModelTools.jl/blob/master/src/ordered_spaces.jl
# these functions return vectors of states, actions and observations, ordered according to stateindex, actionindex, etc.
ordered_actions(mdp::MDP) = ordered_vector(actiontype(typeof(mdp)), a->actionindex(mdp,a), actions(mdp), "action")

ordered_states(mdp::MDP) = ordered_vector(statetype(typeof(mdp)), s->stateindex(mdp,s), states(mdp), "state")


function ordered_vector(T::Type, index::Function, space, singular, plural=singular*"s")
    len = length(space)
    a = Array{T}(undef, len)
    gotten = falses(len)
    for x in space
        id = index(x)
        if id > len || id < 1
            error("""
                  $(singular)index(...) returned an index that was out of bounds for $singular $x.
                  index was $id.
                  n_$plural(...) was $len.
                  """) 
        end
        a[id] = x
        gotten[id] = true
    end
    if !all(gotten)
        missing = findall(.!gotten)
        @warn """
             Problem creating an ordered vector of $plural in ordered_$plural(...). There is likely a mistake in $(singular)index(...) or n_$plural(...).
             n_$plural(...) was $len.
             $plural corresponding to the following indices were missing from $plural(...): $missing
             """
    end
    return a
end


#Pkg.add(url="https://github.com/JuliaPOMDP/POMDPModelTools.jl/blob/master/src/distributions/sparse_cat.jl")
#abstract type MDP{S,A} end


struct SparseCat{V, P}
    vals::V
    probs::P
end

function rand(rng::AbstractRNG, d::SparseCat)
    r = sum(d.probs)*rand(rng)
    tot = zero(eltype(d.probs))
    for (v, p) in d
        tot += p
        if r < tot
            return v
        end
    end
    if sum(d.probs) <= 0.0
        error("""
              Tried to sample from a SparseCat distribution with probabilities that sum to $(sum(d.probs)).
              vals = $(d.vals)
              probs = $(d.probs)
              """)
    end
    error("Error sampling from SparseCat distribution with vals $(d.vals) and probs $(d.probs)") # try to help with type stability
end

# slow linear search :(
function pdf(d::SparseCat, s)
    for (v, p) in d
        if v == s
            return p
        end
    end
    return zero(eltype(d.probs))
end

function pdf(d::SparseCat{V,P}, s) where {V<:AbstractArray, P<:AbstractArray}
    for (i,v) in enumerate(d.vals)
        if v == s
            return d.probs[i]
        end
    end
    return zero(eltype(d.probs))
end



support(d::SparseCat) = d.vals

weighted_iterator(d::SparseCat) = d

# iterator for general SparseCat
# this has some type stability problems
function Base.iterate(d::SparseCat)
    val, vstate = iterate(d.vals)
    prob, pstate = iterate(d.probs)
    return ((val=>prob), (vstate, pstate))
end
function Base.iterate(d::SparseCat, dstate::Tuple)
    vstate, pstate = dstate
    vnext = iterate(d.vals, vstate)
    pnext = iterate(d.probs, pstate)
    if vnext == nothing || pnext == nothing
        return nothing 
    end
    val, vstate_next = vnext
    prob, pstate_next = pnext
    return ((val=>prob), (vstate_next, pstate_next))
end

# iterator for SparseCat with indexed members
const Indexed = Union{AbstractArray, Tuple, NamedTuple}

function Base.iterate(d::SparseCat{V,P}, state::Integer=1) where {V<:Indexed, P<:Indexed}
    if state > length(d)
        return nothing 
    end
    return (d.vals[state]=>d.probs[state], state+1)
end

Base.length(d::SparseCat) = min(length(d.vals), length(d.probs))
Base.eltype(D::Type{SparseCat{V,P}}) where {V, P} = Pair{eltype(V), eltype(P)}
sampletype(D::Type{SparseCat{V,P}}) where {V, P} = eltype(V)
Random.gentype(D::Type{SparseCat{V,P}}) where {V, P} = eltype(V)

function mean(d::SparseCat)
    vsum = zero(eltype(d.vals))
    for (v, p) in d
        vsum += v*p
    end
    return vsum/sum(d.probs)
end

function mode(d::SparseCat)
    bestp = zero(eltype(d.probs))
    bestv = first(d.vals)
    for (v, p) in d
        if p >= bestp
            bestp = p
            bestv = v
        end 
    end
    return bestv
end

Base.show(io::IO, m::MIME"text/plain", d::SparseCat) = showdistribution(io, m, d, title="SparseCat distribution")

##Pkg.add(url="https://github.com/JuliaPOMDP/POMDPModelTools.jl/blob/master/src/distributions/sparse_cat.jl")

#import Pkg;
#Pkg.add("POMDPPolicies")

#using POMDPPolicies


abstract type Policy end
#abstract type MDP{S,A} end


struct ValueIterationPolicy{Q<:AbstractMatrix, U<:AbstractVector, P<:AbstractVector, A, M<:MDP} <: Policy
    qmat::Q
    util::U 
    policy::P 
    action_map::Vector{A}
    include_Q::Bool 
    mdp::M
end

# constructor with an optinal initial value function argument
function ValueIterationPolicy(mdp::MDP;
                              utility::AbstractVector{Float64}=zeros(length(states(mdp))),
                              policy::AbstractVector{Int64}=zeros(Int64, length(states(mdp))),
                              include_Q::Bool=true)
    ns = length(states(mdp))
    na = length(actions(mdp))
    @assert length(utility) == ns "Input utility dimension mismatch"
    @assert length(policy) == ns "Input policy dimension mismatch"
    action_map = ordered_actions(mdp)
    include_Q ? qmat = zeros(ns,na) : qmat = zeros(0,0)
    return ValueIterationPolicy(qmat, utility, policy, action_map, include_Q, mdp)
end

# constructor for solved q, util and policy
function ValueIterationPolicy(mdp::MDP, q::AbstractMatrix{F}, util::AbstractVector{F}, policy::Vector{Int64}) where {F}
    action_map = ordered_actions(mdp)
    include_Q = true
    return ValueIterationPolicy(q, util, policy, action_map, include_Q, mdp)
end

# constructor for default Q-matrix
function ValueIterationPolicy(mdp::MDP, q::AbstractMatrix{F}) where {F}
    (ns, na) = size(q)
    p = zeros(Int64, ns)
    u = zeros(ns)
    for i = 1:ns
        p[i] = argmax(q[i,:])
        u[i] = maximum(q[i,:])
    end
    action_map = ordered_actions(mdp)
    include_Q = true
    return ValueIterationPolicy(q, u, p, action_map, include_Q, mdp)
end

# returns the fields of the policy type
function locals(p::ValueIterationPolicy)
    return (p.qmat,p.util,p.policy,p.action_map)
end

function action(policy::ValueIterationPolicy, s::S) where S
    sidx = stateindex(policy.mdp, s)
    aidx = policy.policy[sidx]
    return policy.action_map[aidx]
end

function value(policy::ValueIterationPolicy, s::S) where S
    sidx = stateindex(policy.mdp, s)
    policy.util[sidx]
end

value(policy::ValueIterationPolicy, s, a) = actionvalues(policy, s)[actionindex(policy.mdp, a)]














function showpolicy(io::IO, mime::MIME"text/plain", m::MDP, p::Policy; pre=" ", kwargs...)
    slist = nothing
    truncated = false
    limited = get(io, :limit, false)
    rows = first(get(io, :displaysize, displaysize(io)))
    rows -= 3 # Yuck! This magic number is also in Base.print_matrix
    try
        if limited && length(states(m)) > rows
            slist = collect(take(states(m), rows-1))
            truncated = true
        else
            slist = collect(states(m))
        end
    catch ex
        @info("""Unable to pretty-print policy:
              $(sprint(showerror, ex))
              """)
        show(io, mime, m)
        return show(io, mime, p)
    end
    showpolicy(io, mime, slist, p; pre=pre, kwargs...)
    if truncated
        print(io, '\n', pre, "…")
    end
end

function showpolicy(io::IO, mime::MIME"text/plain", slist::AbstractVector, p::Policy; pre::AbstractString=" ")
    S = eltype(slist)
    sa_con = IOContext(io, :compact => true)

    if !isempty(slist)
        # print first element without a newline
        print(io, pre)
        print_sa(sa_con, first(slist), p, S)

        # print all other elements
        for s in slist[2:end]
            print(io, '\n', pre)
            print_sa(sa_con, s, p, S)
        end
    end
end

showpolicy(io::IO, m::Union{MDP,AbstractVector}, p::Policy; kwargs...) = showpolicy(io, MIME("text/plain"), m, p; kwargs...)
showpolicy(m::Union{MDP,AbstractVector}, p::Policy; kwargs...) = showpolicy(stdout, m, p; kwargs...)

function print_sa(io::IO, s, p::Policy, S::Type)
    show(IOContext(io, :typeinfo => S), s)
    print(io, " -> ")
    try
        show(io, action(p, s))
    catch ex
        showerror(IOContext(io, :limit=>true), ex)
    end
end






function Base.show(io::IO, mime::MIME"text/plain", p::ValueIterationPolicy)
    println(io, "ValueIterationPolicy:")
    ds = get(io, :displaysize, displaysize(io))
    ioc = IOContext(io, :displaysize=>(first(ds)-1, last(ds)))
    showpolicy(ioc, mime, p.mdp, p)
end