##ordered spaces

#https://github.com/JuliaPOMDP/POMDPModelTools.jl/blob/master/src/ordered_spaces.jl
# these functions return vectors of states, actions and observations, ordered according to stateindex, actionindex, etc.
ordered_actions(mdp::MDP) = ordered_vector(actiontype(typeof(mdp)), a->actionindex(mdp,a), actions(mdp), "action")

ordered_states(mdp::MDP) = ordered_vector(statetype(typeof(mdp)), s->stateindex(mdp,s), states(mdp), "state")


function ordered_vector(T::Type, index::Function, space, singular, plural=singular*"s")
    len = length(space)
    a = Array{T}(undef, len)
    gotten = falses(len)
    for x in space
        id = index(x)
        if id > len || id < 1
            error("""
                  $(singular)index(...) returned an index that was out of bounds for $singular $x.
                  index was $id.
                  n_$plural(...) was $len.
                  """) 
        end
        a[id] = x
        gotten[id] = true
    end
    if !all(gotten)
        missing = findall(.!gotten)
        @warn """
             Problem creating an ordered vector of $plural in ordered_$plural(...). There is likely a mistake in $(singular)index(...) or n_$plural(...).
             n_$plural(...) was $len.
             $plural corresponding to the following indices were missing from $plural(...): $missing
             """
    end
    return a
end
