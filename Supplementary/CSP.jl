### A Pluto.jl notebook ###
# v0.14.7

using Markdown
using InteractiveUtils

# ╔═╡ c37d5551-a82b-440b-b4ea-6d8d326218c0
using Pkg; Pkg.add("PlutoUI"); using PlutoUI

# ╔═╡ 4e2f1ae6-c21d-11eb-162b-f99d197a14f9
begin
@enum Domain one two three four
	
mutable struct Variable
    name::AbstractString
    domain::Array{Any,1}
    edges::Array{Any,1}
end
end

# ╔═╡ 0547c4a3-4bc6-4f76-b735-9770691133d8
Domain

# ╔═╡ 61707883-3b3b-4064-a477-c08413edb8af
begin
# Remove value from the neigbours/edges
function domain_wipeout(value, node)
    for i in 1:size(node.edges)[1]
        deleteat!(node.edges[i].domain, findall(x->x==value,node.edges[i].domain))
    end
end
# Removes all matches of value from list
function clean_node_domain(the_list, val)
    deleteat!(the_list, findall(x->x==val,the_list))
end
end

# ╔═╡ b991999d-ac77-4497-8e4c-454c37597d90
begin
	
#define variables
	
x1 = Variable("x1", [one, two, three, four], [])
x2 = Variable("x2", [one, two, three, four], [])
x3 = Variable("x3", [one, two], [])
x4 = Variable("x4", [one, two, three, four], [])
x5 = Variable("x5", [one, two, three, four], [])
x6 = Variable("x6", [one, two, three, four], [])
x7 = Variable("x7", [one, two, three, four], [])
	


# Set the neighbours/edges
	
x1.edges = [x2, x3]
x2.edges = [x1, x5]
x3.edges = [x4, x1]
x4.edges = [x6, x5, x3]
x5.edges = [x2, x4, x6]
x6.edges = [x4, x5, x7]
x7.edges = [x6]
	

end

# ╔═╡ 1c15daef-bcbd-4927-9dd8-8248a910dbc5
function solve()
	
	out = []
	
search_tree = [x1,x2,x3,x4,x5,x6,x7]
	
for node_index in 1:size(search_tree)[1]
    node = search_tree[node_index]
		
    c = node.domain[1]
		
    domain_wipeout(c, node)
    
		
    for _c_index in 1:size(node.domain)[1]-1
        _c = node.domain[_c_index]
        
        if _c != c
            clean_node_domain(node.domain, _c)
        end
    end
			
		push!(out,string("",node.name, " => ", node.domain[1],""))
			#=with_terminal() do
			
				println(node.name, " -> ", node.domain[1])
	end=#
		
end
	return out
end

# ╔═╡ 6c16272b-e655-4f09-8b79-a4cf49dddc66
terminall = solve();

# ╔═╡ 1e00d782-051d-458d-8326-5c1a4c0af4e0
with_terminal() do
	for i in terminall
		println(i)
	end
end

# ╔═╡ Cell order:
# ╠═c37d5551-a82b-440b-b4ea-6d8d326218c0
# ╠═4e2f1ae6-c21d-11eb-162b-f99d197a14f9
# ╠═0547c4a3-4bc6-4f76-b735-9770691133d8
# ╠═61707883-3b3b-4064-a477-c08413edb8af
# ╠═b991999d-ac77-4497-8e4c-454c37597d90
# ╠═1c15daef-bcbd-4927-9dd8-8248a910dbc5
# ╠═6c16272b-e655-4f09-8b79-a4cf49dddc66
# ╠═1e00d782-051d-458d-8326-5c1a4c0af4e0
