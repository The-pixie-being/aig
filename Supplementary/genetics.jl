### A Pluto.jl notebook ###
# v0.14.7

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : missing
        el
    end
end

# ╔═╡ df4b16d7-1c06-418f-bde9-0884c9cc53ac
begin
	using Pkg; Pkg.add("PlutoUI")
using Random
using PlutoUI
end

# ╔═╡ ced7519b-a21b-425c-8945-8edb0a33bf2f
md"###### Author: Bill Ikela
[gitlab repo](https://gitlab.com/The-pixie-being/aig/-/tree/main/Supplementary)
"

# ╔═╡ 47aaa877-7e30-4fd1-b663-280d835b847e
md"### Install packages like PlutoUI"

# ╔═╡ f1aa52bb-4be8-4cda-8cab-40465b1d7609
md"### The Selection function"

# ╔═╡ 4c8365d0-e2e8-11eb-0d22-512f26688c9f
begin
	function selection(pop, scores, k=3)
    # first random selection
	selection_ix = rand(1:length(pop)) #or for multidimensional arrays :  rand(1:size([1,2,3])[1]) #get random index of a population
	for ix in rand(randsubseq(1:length(pop),1),k-1) #randomize index value
		# check if better (e.g. perform a tournament)
		if scores[ix] < scores[selection_ix]
			selection_ix = ix
        end
    end
	return pop[selection_ix]
end
end

# ╔═╡ d649816b-b9be-4d2e-93a5-21ade77d9d35
md"### Crossover Function"

# ╔═╡ 1d8184d4-4d4b-4dbc-8b6f-db09a649076e
begin
	function crossover(p1, p2, r_cross)
	# children are copies of parents by default
	c1, c2 = deepcopy(p1),deepcopy(p2)
	# check for recombination
	if rand() < r_cross
		# select crossover point that is not on the end of the string
       # println(length(p1)-1)
		pt =  rand(1:length(p1)-1)
        #pt = 5
        #println(p2)
		# perform crossover
		c1 = vcat(p1[1:pt],p2[pt+1:length(p2)])
		c2 = vcat(p2[1:pt],p1[pt+1:length(p1)])
    end
	return [c1, c2]
end
end

# ╔═╡ c4693d71-74e2-44f5-a146-b55e29c2de28
md"### Mutation Function"

# ╔═╡ 1b98a670-83d8-44c7-a73f-091e8fa0e15e
begin
	# mutation operator
function mutation(bitstring, r_mut)
	for i = 1:length(bitstring)
		# check for a mutation
		if rand() < r_mut
			# flip the bit
			bitstring[i] = 1 - bitstring[i]
            #splice!(bitstring,i,1-bitstring[i])
        end
    end
end
end

# ╔═╡ ea1e60ca-1fba-4d1d-addd-16bc518a7d11
md"### The Genetic Algorithm"

# ╔═╡ 7eba2138-9c56-4faf-9181-a004a37d04bf
begin
	function genetic_algorithm(objective, n_bits, n_iter, n_pop, r_cross, r_mut)
	# initial population of random bitstring
    pop = [bitrand(n_bits) for _ = 1:n_pop]
	# keep track of best solution
	best, best_eval = 0, objective(pop[1])
	# enumerate generations
	for gen in range(1,stop=n_iter)
		# evaluate all candidates in the population
		scores = [objective(c) for c in pop]
        
        
		# check for new best solution
		for i in range(1,stop=n_pop)
			if scores[i] < best_eval
				best, best_eval = pop[i], scores[i]
				#@printf("generation:%d, best %s = %.3f", gen,  pop[i], scores[i])
                println("generation ::$gen\n new best solution :: ",pop[i],"\n fitness = ",scores[i])
                println()
            end
        end
		# select parents
		selected = [selection(pop, scores) for _ in 1:n_pop]
		# create the next generation
        
		children = []
		for i = 1:2:length(pop)
			# get selected parents in pairs
    
           # println("i=",i)
			p1, p2 = selected[i], selected[i+1]
			# crossover and mutation
            #println(length(p1))
            
			for c in crossover(p1, p2, r_cross)
				# mutation
				mutation(c, r_mut)
				# store for next generationprintln(c)
                    #append!(children,c)
                push!(children,c)
            end
            
        end
		# replace population
		pop = children
    end
    
	return [best, best_eval]
end
                    
end

# ╔═╡ 2294fe09-4c3e-4101-9e3b-357302073bc6
md"### The Fitness Function"

# ╔═╡ a6a56e99-ca2d-49ac-9e0b-2a5568b99b2d
# fitness/objective function
fitness(x)= -sum(x)

# ╔═╡ 764cd95a-6474-4824-8e77-f3d899f499f5
md"#### Input iteration value"

# ╔═╡ 269cfe67-e6b9-49d8-bd2d-45e15d7e3dc0
@bind iter TextField()

# ╔═╡ e05c1761-eaa7-4db2-b537-e908339be6ec
md"#### Input bitstring count"

# ╔═╡ c16feabe-d9b3-4b12-9caf-2cfdebcebbf8
@bind bits TextField()

# ╔═╡ 8ee9ebb5-54c1-4341-a30c-b0860af4cbb4
md"#### Input population count"

# ╔═╡ 615c51f9-8285-4865-8cb9-7850a728d525
@bind pop TextField()

# ╔═╡ fb11226f-c57c-4d58-a2bc-e94adc25a898
md"#### Input cross rate"

# ╔═╡ feebd083-7b67-4828-92d7-7122e0630838
@bind cross TextField()

# ╔═╡ 5cc11655-8d87-4198-beb4-31bc8acc4155
md"#### Input mutation rate"

# ╔═╡ e017d66e-701e-481e-8df7-032677642c57
@bind mut TextField()

# ╔═╡ b405b942-7395-4bc0-a7df-045d47af0fc1
begin
	# genetic algorithm search of the one max optimization problem


 
# define the total iterations
if isempty(iter)
	n_iter = 100
	else
		n_iter = parse(Int64,iter)
	end
	
	
# bits
	if isempty(bits)
n_bits = 20
		else
		n_bits = parse(Int64,bits)
	end
	
# define the population size
	if isempty(pop)
n_pop = 100
		else
		n_pop = parse(Int64,pop)
	end
	
# crossover rate
	if isempty(cross)
r_cross = 0.9
		else
		r_cross = parse(Float64,cross)/100

	end
	# mutation rate
		if isempty(mut)
r_mut = 1.0 / float(n_bits)
# perform the genetic algorithm search
		else
		r_mut = parse(Int64,mut)/n_bits
	end
	
	with_terminal() do
best, score = genetic_algorithm(fitness, n_bits, n_iter, n_pop, r_cross, r_mut)
#best, score = genetic_algorithm(onemax, 5, 10, n_pop, r_cross, r_mut)

		
println("\n\n------------ Overall Best solution --------------\n\n", best,"\nfitness => ",score)
	end
	
end

# ╔═╡ Cell order:
# ╠═ced7519b-a21b-425c-8945-8edb0a33bf2f
# ╠═47aaa877-7e30-4fd1-b663-280d835b847e
# ╠═df4b16d7-1c06-418f-bde9-0884c9cc53ac
# ╠═f1aa52bb-4be8-4cda-8cab-40465b1d7609
# ╠═4c8365d0-e2e8-11eb-0d22-512f26688c9f
# ╠═d649816b-b9be-4d2e-93a5-21ade77d9d35
# ╠═1d8184d4-4d4b-4dbc-8b6f-db09a649076e
# ╠═c4693d71-74e2-44f5-a146-b55e29c2de28
# ╠═1b98a670-83d8-44c7-a73f-091e8fa0e15e
# ╠═ea1e60ca-1fba-4d1d-addd-16bc518a7d11
# ╠═7eba2138-9c56-4faf-9181-a004a37d04bf
# ╠═2294fe09-4c3e-4101-9e3b-357302073bc6
# ╠═a6a56e99-ca2d-49ac-9e0b-2a5568b99b2d
# ╠═764cd95a-6474-4824-8e77-f3d899f499f5
# ╠═269cfe67-e6b9-49d8-bd2d-45e15d7e3dc0
# ╠═e05c1761-eaa7-4db2-b537-e908339be6ec
# ╠═c16feabe-d9b3-4b12-9caf-2cfdebcebbf8
# ╠═8ee9ebb5-54c1-4341-a30c-b0860af4cbb4
# ╠═615c51f9-8285-4865-8cb9-7850a728d525
# ╠═fb11226f-c57c-4d58-a2bc-e94adc25a898
# ╠═feebd083-7b67-4828-92d7-7122e0630838
# ╠═5cc11655-8d87-4198-beb4-31bc8acc4155
# ╠═e017d66e-701e-481e-8df7-032677642c57
# ╠═b405b942-7395-4bc0-a7df-045d47af0fc1
